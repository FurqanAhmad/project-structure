//
//  ForgotPasswordVC.swift
//  Rezeptrechner
//
//  Created by Nouman on 10/25/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftValidator

class ForgotPasswordVC: BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!
    
    //MARK: - Class Properties
    let validator = Validator()
    
    override func setupGUI() {
        self.validator.registerField(emailTF, rules: [MinLengthRule(length: 5, message: "")])

    }
}

//MARK: - Class Constructor
extension ForgotPasswordVC {
    
    class func forgotPasswordVC() -> ForgotPasswordVC {
        return UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
    }
}

//MARK: - IBActions
extension ForgotPasswordVC {
    
    @IBAction func sendEmailBtnPressed(_ sender: Any) {
        validator.validate(self)
    }
}

// MARK: Validation Extension
extension ForgotPasswordVC: ValidationDelegate {
    
    func validationSuccessful() {
        
        if matchPattren(CheckString: emailTF.text!) {
            // valid case
        } else {
            //invalid Case
        }
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            
            if let textField = field as? UITextField {
                textField.shake()
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
}
