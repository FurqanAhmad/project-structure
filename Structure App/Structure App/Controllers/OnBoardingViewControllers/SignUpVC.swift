//
//  SignUpVC.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/26/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftValidator

class SignUpVC: BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var userNameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var rePasswordTF: SkyFloatingLabelTextField!
    
    //MARK: - Class Properties
    let validator = Validator()
    
    override func setupGUI() {
        validateTextField()
    }
}

//MARK: - Class Constructor
extension SignUpVC {
    
    class func signUpVC() -> SignUpVC {
        return UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    }
}

//MARK: - IBActions
extension SignUpVC {
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
       

        validator.validate(self)
    }
}

//MARK: - Class Methods
extension SignUpVC {
    
    fileprivate func validateTextField() {
        
        self.validator.registerField(userNameTF, rules: [MinLengthRule(length: 2, message: "")])
        self.validator.registerField(emailTF, rules: [MinLengthRule(length: 6, message: "")])
        self.validator.registerField(rePasswordTF, rules: [MinLengthRule(length: 6, message: "")])
        self.validator.registerField(passwordTF, rules: [MinLengthRule(length: 6, message: "") , ConfirmationRule.init(confirmField: rePasswordTF, message: "Password Missmatch")])
        
    }
    
}

// MARK: Validation Extension
extension SignUpVC: ValidationDelegate {
    
    func validationSuccessful() {
        
        if matchPattren(CheckString: emailTF.text!) {
            // valid case
        } else {
            //invalid Case
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            
            if let textField = field as? UITextField {
                //invalid Case
                textField.shake()

            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
    
    }

//MARK: - UITextFieldDelegate
extension SignUpVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
