//
//  SignInVC.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/24/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftValidator

class SignInVC: BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    
    //MARK: - Class Properties
    let validator = Validator()

    override func setupGUI() {
        validateTextField()
    }
}

//MARK: - Class Constructor
extension SignInVC {
    
    class func signInVC() -> SignInVC {
        return UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
    }
}

//MARK: - IBActions
extension SignInVC {
    
    @IBAction func signIngBtnPressed(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    @IBAction func forgotPasswordBtnPressed(_ sender: Any) {
        
        // loading forgot password screen
        let forgotPasswordVC = ForgotPasswordVC.forgotPasswordVC()
        forgotPasswordVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(forgotPasswordVC, animated: true)
        
    }
}

//MARK: - Class Methods
extension SignInVC {
    
    fileprivate func validateTextField() {
        
        validator.registerField(emailTF, rules: [MinLengthRule(length: 6, message: "")])
        validator.registerField(passwordTF, rules: [MinLengthRule(length: 6, message: "")])
        
    }
}

// MARK: Validation Extension
extension SignInVC: ValidationDelegate {
    
    func validationSuccessful() {
        
        if matchPattren(CheckString: emailTF.text!) {
            // valid case
        } else {
            //invalid Case
        }
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            
            if let textField = field as? UITextField {
                textField.shake()
            }
            
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK: - UITextFieldDelegate
extension SignInVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
