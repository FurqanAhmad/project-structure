//
//  WelcomeVC.swift
//  Rezeptrechner
//
//  Created by Furqan on 10/24/18.
//  Copyright © 2018 faari27@gmail.com. All rights reserved.
//

import UIKit

class WelcomeVC: BaseVC {}

//MARK: - Class Constructor
extension WelcomeVC {
    
    class func welcomeVC() -> WelcomeVC {
        return UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
    }
}

//MARK: - IBActions
extension WelcomeVC {
    
    @IBAction func loginBtnAction(_ sender: Any) {

        let signInVC = SignInVC.signInVC()
        signInVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(signInVC, animated: true)
        
    }
    
    @IBAction func registerationBtnAction(_ sender: Any) {

        let signUpVC = SignUpVC.signUpVC()
        signUpVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(signUpVC, animated: true)

    }
}
